module gitlab.com/moneropay/moneropay/v2

go 1.19

require (
	github.com/gabstv/httpdigest v0.0.0-20230306144402-1057ac3638b3
	github.com/go-chi/chi/v5 v5.0.10
	github.com/golang-migrate/migrate/v4 v4.16.2
	github.com/jackc/pgx/v5 v5.4.3
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/namsral/flag v1.7.4-pre
	github.com/rs/zerolog v1.30.0
	gitlab.com/moneropay/go-monero v1.1.0
	golang.org/x/net v0.14.0
)

require (
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/jackc/pgerrcode v0.0.0-20220416144525-469b46aa5efa // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	go.uber.org/atomic v1.11.0 // indirect
	golang.org/x/crypto v0.12.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
	golang.org/x/text v0.12.0 // indirect
)
